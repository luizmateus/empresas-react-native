export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
}

export function signInSuccess(accessToken, client, uid) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { accessToken, client, uid },
  };
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  };
}
