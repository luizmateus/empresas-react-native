import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import api from '~/services/api';

import { signInSuccess, signFailure } from './actions';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    const response = yield call(api.post, 'api/v1/users/auth/sign_in', {
      email,
      password,
    });

    const { 'access-token': accessToken, client, uid } = response.headers;

    api.defaults.headers = {
      'access-token': accessToken,
      client,
      uid,
    };

    yield put(signInSuccess(accessToken, client, uid));
  } catch (err) {
    Alert.alert(
      'Falha na autenticação',
      'Houve um erro no login, verifique seus dados'
    );
    yield put(signFailure());
  }
}

export function setToken({ payload }) {
  if (!payload) return;

  const { accessToken, client, uid } = payload.auth;

  if (accessToken) {
    api.defaults.headers = {
      'access-token': accessToken,
      client,
      uid,
    };
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
]);
