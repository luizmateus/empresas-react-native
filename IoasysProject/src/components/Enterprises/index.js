import React from 'react';

import { User, Name, Bio, ProfileButton, ProfileButtonText } from './styles';

export default function Enterprises({ data, onPress }) {
  return (
    <User>
      <Name>{data.enterprise_name}</Name>
      <Bio>{data.description}</Bio>
      <ProfileButton onPress={onPress}>
        <ProfileButtonText>Show more details</ProfileButtonText>
      </ProfileButton>
    </User>
  );
}
