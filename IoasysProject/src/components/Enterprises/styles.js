import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';

export const User = styled.View`
  align-items: center;
  margin: 0 20px 30px;
  border-bottom-width: 1px;
  border-color: #eee;
`;

export const Name = styled.Text`
  font-size: 14px;
  color: #f6f8fa;
  font-weight: bold;
  margin-top: 4px;
  text-align: center;
`;

export const Bio = styled.Text.attrs({
  numberOfLines: 2,
})`
  font-size: 13px;
  line-height: 18px;
  color: white;
  margin-top: 5px;
  text-align: center;
`;

export const ProfileButton = styled(RectButton)`
  margin-top: 10px;
  align-self: stretch;
  border-radius: 4px;
  border-style: solid;
  border-color: #e1e4e8;
  border-width: 2px;
  background: #f6f8fa;
  justify-content: center;
  align-items: center;
  height: 36px;
  margin-bottom: 20px;
`;

export const ProfileButtonText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #586069;
  text-transform: uppercase;
`;
