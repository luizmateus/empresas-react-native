import React, { forwardRef } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Container, TInput } from './styles';

function Input({ style, icon, clear, value, ...rest }, ref) {
  return (
    <Container style={style}>
      {icon && <Icon name={icon} size={20} color="rgba(255, 255, 255, 0.6)" />}
      <TInput {...rest} ref={ref} value={value} />
      {value !== '' && (
        <TouchableWithoutFeedback onPress={clear}>
          <Icon name="clear" size={20} color="rgba(255, 255, 255, 0.6)" />
        </TouchableWithoutFeedback>
      )}
    </Container>
  );
}

Input.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  icon: PropTypes.string,
  clear: PropTypes.func,
  value: PropTypes.string.isRequired,
};

Input.defaultProps = {
  style: {},
  icon: null,
  clear: () => {},
};

export default forwardRef(Input);
