import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

export default styled(LinearGradient).attrs({
  colors: ['#e51f6b', '#ec5e90'],
})`
  flex: 1;
`;
