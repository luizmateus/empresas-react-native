import React, { useEffect, useState } from 'react';
import { Text } from 'react-native';
import Background from '~/components/Background';
import { Container, Title } from './styles';
import api from '../../services/api';

export default function Enterprise() {
  const [enterprise, setEnterprise] = useState({});

  useEffect(() => {
    async function loadEnterprise() {
      const response = await api.get('/api/v1/enterprises');

      setEnterprise(response.data);
    }
    loadEnterprise();
  }, []);

  return (
    <Background>
      <Container>
        <Title>AllRide</Title>
        <Text
          style={{
            fontSize: 16,
            lineHeight: 24,
            fontWeight: 'normal',
            letterSpacing: 0.1,
            color: '#fff',
            margin: 12,
          }}
        >
          Urbanatika is a socio-environmental company with economic impact,
          creator of the agro-urban industry. We want to involve people in the
          processes of healthy eating, recycling and reuse of organic waste and
          the creation of citizen green areas. With this we are creating smarter
          cities from the people and at the same time the forest city.
          Urbanatika, Agro-Urban Industry
        </Text>
        <Text
          style={{
            fontSize: 16,
            lineHeight: 24,
            fontWeight: 'normal',
            letterSpacing: 0.1,
            color: '#fff',
            margin: 12,
          }}
        >
          Enterprise type: Software
        </Text>
        <Text
          style={{
            fontSize: 16,
            lineHeight: 24,
            fontWeight: 'normal',
            letterSpacing: 0.1,
            color: '#fff',
            margin: 12,
          }}
        >
          City: Santiago
        </Text>
        <Text
          style={{
            fontSize: 16,
            lineHeight: 24,
            fontWeight: 'normal',
            letterSpacing: 0.1,
            color: '#fff',
            margin: 12,
          }}
        >
          Country: Chile
        </Text>
      </Container>
    </Background>
  );
}
