import React, { useEffect, useState } from 'react';
import { withNavigationFocus } from 'react-navigation';
import api from '~/services/api';
import Background from '~/components/Background';
import Enterprises from '~/components/Enterprises';
import { Container, Title, List, SearchBar } from './styles';

function Dashboard({ navigation }) {
  const [enterprises, setEnterprises] = useState([]);
  const [text, setText] = useState('');

  useEffect(() => {
    async function loadEnterprises() {
      const response = await api.get('/api/v1/enterprises');

      setEnterprises(response.data.enterprises);
    }
    loadEnterprises();
  }, []);

  return (
    <Background>
      <Container>
        <Title>Enterprises</Title>
        <SearchBar
          icon="search"
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Search for name"
          returnKeyType="send"
          value={text}
          onChangeText={setText}
          clear={() => setText('')}
        />
        <List
          data={enterprises}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => (
            <Enterprises
              data={item}
              onPress={() => navigation.navigate('Enterprise')}
            />
          )}
        />
      </Container>
    </Background>
  );
}

export default withNavigationFocus(Dashboard);
