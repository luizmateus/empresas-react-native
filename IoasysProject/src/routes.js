import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import SignIn from './screens/SignIn';
import Home from './screens/Home';
import Enterprise from './screens/Enterprise';

export default (isSigned = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          SignIn,
        }),
        Home: createSwitchNavigator({
          Home,
          Enterprise,
        }),
      },
      {
        initialRouteName: isSigned === true ? 'Home' : 'Sign',
      }
    )
  );
